export let mainPage = 'https://sibdev.pro/'
export let practice = 'http://sibdev.pro/practice/'
export let portfolioPage = 'https://sibdev.pro/portfolio/'
export let servicesPage = 'https://sibdev.pro/services'

// Проверочный массив футера
export let itemArray = [
  
    [
        ['/site',],
        ['/landing', '/corporate-site', '/online-store', '/card-site', '/adaptive-site',]
    ],
  
    [
        ['/app',],
        ['/spa', '/chat-bot', '/mobile', '/script',]
    ],
  
    [
        ['/saas',],
        ['/highload', '/online-auction', '/tender-place', '/agregator',]
    ],
  
    [
        ['/outsourcing',],
        ['/markup', '/frontend', '/backend',]
    ],
  
    [
        ['/maintenance', '/design', '/portfolio/',],
        []
    ],
  
    [
        ['/studio',],
        ['/studio', '/vacancies',]
    ],
  
]

// Массив для страницы Практика
export let questionArray = [
    'Практика будет проходить в период с 15 июля по 19 августа',
    'Практика будет проходить полностью удаленно',
    'Повторить основы программирования на выбранной технологии',
    'Если у вас Windows, то мы рекомендуем поставить Linux второй системой',
    'Да, нам нужен заполненный с вашей стороны экземпляр документа',
    'Да, конечно! Практика подойдет всем, кто хочет построить карьеру в IT',
    'Полезный опыт, проект в портфолио и мерч от Sibdev',
]

// Массив объектов с названиями и URL'ами страниц
export let pagesArray = [
    {name: 'Главная', url: 'http://sibdev.pro/'},
    {name: 'Услуги', url: 'http://sibdev.pro/services'},
    {name: 'Портфолио', url: 'http://sibdev.pro/portfolio/'},
    {name: 'О нас', url: 'http://sibdev.pro/studio'},
    {name: 'Контакты', url: 'http://sibdev.pro/contacts'},
    {name: 'Практика', url: 'http://sibdev.pro/practice/'},
    {name: 'Сайты', url: 'http://sibdev.pro/site'},
    {name: 'Лендинги', url: 'http://sibdev.pro/landing'},
    {name: 'Корпоративные сайты', url: 'http://sibdev.pro/corporate-site'},
    {name: 'Интернет-магазины', url: 'http://sibdev.pro/online-store'},
    {name: 'Сайты-визитки', url: 'http://sibdev.pro/card-site'},
    {name: 'Адаптивные сайты', url: 'http://sibdev.pro/adaptive-site'},
    {name: 'Веб-сервисы', url: 'http://sibdev.pro/saas'},
    {name: 'Высоконагруженные сервисы', url: 'http://sibdev.pro/highload'},
    {name: 'Интернет-аукционы', url: 'http://sibdev.pro/online-auction'},
    {name: 'Тендерные площадки', url: 'http://sibdev.pro/tender-place'},
    {name: 'Агрегаторы', url: 'http://sibdev.pro/agregator'},
    {name: 'Чат-боты', url: 'http://sibdev.pro/chat-bot'},
    {name: 'Мобильные приложения', url: 'http://sibdev.pro/mobile'},
    {name: 'Веб-приложения (SPA)', url: 'http://sibdev.pro/spa'},
    {name: 'Скрипты', url: 'http://sibdev.pro/script'},
    {name: 'Сопровождение', url: 'http://sibdev.pro/maintenance'},
    {name: 'Дизайн', url: 'http://sibdev.pro/design'},
    {name: 'ИТ-аутсорс', url: 'http://sibdev.pro/outstaffing'},
    {name: 'Верстка', url: 'http://sibdev.pro/markup'},
    {name: 'Frontend', url: 'http://sibdev.pro/frontend'},
    {name: 'Backend', url: 'http://sibdev.pro/backend'},
    {name: 'Блог', url: 'https://sibdev.pro/blog/'},
]

// Массив для поверки ссылок страницы Услуги
export let bodyLinksArray = [

    ['/site', '/landing', '/corporate-site', '/online-store', '/card-site', '/adaptive-site',],

    ['/saas', '/highload', '/online-auction', '/tender-place', '/agregator',],

    ['/app', '/chat-bot', '/mobile','/spa', '/script',],

    ['/maintenance', '/design', '/outsourcing', '/markup', '/frontend', '/backend',],

]

// Массив с данными страниц, на которые ссылается страница Услуги
// Логика элемента массива:
// [ URL страницы, Номер в иерархии сайта, Массив с URL'ами для хлебны крошек родителей иерархии, Число параграфов в описании, Число ссылок в портфолио, Название страницы, URL для хлебных крошек (КОСТЫЛЬ) ]

export let dataPagesServicesArray = [

    [
        'https://sibdev.pro/site', 2, ['https://sibdev.pro', 'https://sibdev.pro/services',], 5, 1, 'Сайты', 'https://site.sibdev.pro'
    ],

    [
        'https://sibdev.pro/landing', 3, ['https://sibdev.pro', 'https://sibdev.pro/services', 'https://site.sibdev.pro',], 3, 1, 'Лендинги', 'https://site.sibdev.pro/landing'
    ],

    [
        'https://sibdev.pro/corporate-site', 3, ['https://sibdev.pro', 'https://sibdev.pro/services', 'https://site.sibdev.pro',], 5, 1, 'Корпоративные сайты', 'https://sibdev.pro/corporate-site'
    ],

    [
        'https://sibdev.pro/online-store', 3, ['https://sibdev.pro', 'https://sibdev.pro/services', 'https://site.sibdev.pro',], 3, 2, 'Интернет-магазины', 'https://sibdev.pro/online-store'
    ],

    [
        'https://sibdev.pro/card-site', 3, ['https://sibdev.pro', 'https://sibdev.pro/services', 'https://site.sibdev.pro',], 3, 0, 'Сайты-визитки', 'https://sibdev.pro/card-site'
    ],

    [
        'https://sibdev.pro/adaptive-site', 3, ['https://sibdev.pro', 'https://sibdev.pro/services', 'https://site.sibdev.pro',], 3, 1, 'Адаптивные сайты', 'https://site.sibdev.pro/adaptive-site'
    ],

    [
        'https://sibdev.pro/saas', 2, ['https://sibdev.pro', 'https://sibdev.pro/services',], 3, 1, 'Веб-сервисы', 'https://sibdev.pro/saas'
    ],

    [
        'https://sibdev.pro/highload', 3, ['https://sibdev.pro', 'https://sibdev.pro/services', 'https://sibdev.pro/saas',], 5, 1, 'Высоконагруженные сервисы', 'https://sibdev.pro/highload'
    ],

    [
        'https://sibdev.pro/online-auction', 3, ['https://sibdev.pro', 'https://sibdev.pro/services', 'https://sibdev.pro/saas',], 3, 0, 'Интернет-аукционы', 'https://sibdev.pro/online-auction'
    ],

    [
        'https://sibdev.pro/tender-place', 3, ['https://sibdev.pro', 'https://sibdev.pro/services', 'https://sibdev.pro/saas',], 3, 0, 'Тендерные площадки', 'https://sibdev.pro/tender-place'
    ],

    [
        'https://sibdev.pro/agregator', 3, ['https://sibdev.pro', 'https://sibdev.pro/services', 'https://sibdev.pro/saas',], 3, 0, 'Агрегаторы', 'https://sibdev.pro/agregator'
    ],

    [
        'https://sibdev.pro/app', 2, ['https://sibdev.pro', 'https://sibdev.pro/services',], 3, 0, 'Приложения', 'https://sibdev.pro/app'
    ],

    [
        'https://sibdev.pro/chat-bot', 3, ['https://sibdev.pro', 'https://sibdev.pro/services', 'https://sibdev.pro/app',], 4, 0, 'Чат-боты', 'https://sibdev.pro/chat-bot'
    ],

    [
        'https://sibdev.pro/mobile', 3, ['https://sibdev.pro', 'https://sibdev.pro/services', 'https://sibdev.pro/app',], 4, 0, 'Мобильные приложения', 'https://sibdev.pro/mobile'
    ],

    [
        'https://sibdev.pro/spa', 3, ['https://sibdev.pro', 'https://sibdev.pro/services', 'https://sibdev.pro/app',], 3, 0, 'Веб-приложения (SPA)', 'https://sibdev.pro/spa'
    ],

    [
        'https://sibdev.pro/script', 3, ['https://sibdev.pro', 'https://sibdev.pro/services', 'https://sibdev.pro/app',], 3, 0, 'Скрипты/Утилиты', 'https://sibdev.pro/script'
    ],

    [
        'https://sibdev.pro/maintenance', 1, ['https://sibdev.pro',], 2, 2, 'Сопровождение', 'https://sibdev.pro/maintenance'
    ],

    [
        'https://sibdev.pro/design', 2, ['https://sibdev.pro', 'https://sibdev.pro/services',], 3, 2, 'Дизайн', 'https://sibdev.pro/design'
    ],

    [
        'https://sibdev.pro/outsourcing', 2, ['https://sibdev.pro', 'https://sibdev.pro/services',], 3, 1, 'ИТ-аутсорс', 'https://sibdev.pro/outsourcing'
    ]  ,

    [
        'https://sibdev.pro/markup', 3, ['https://sibdev.pro', 'https://sibdev.pro/services', 'https://sibdev.pro/outsourcing',], 4, 0, 'Верстка', 'https://sibdev.pro/markup'
    ],

    [
        'https://sibdev.pro/frontend', 3, ['https://sibdev.pro', 'https://sibdev.pro/services', 'https://sibdev.pro/outsourcing',], 3, 0, 'Frontend', 'https://sibdev.pro/frontend'
    ],

    [
        'https://sibdev.pro/backend', 3, ['https://sibdev.pro', 'https://sibdev.pro/services', 'https://sibdev.pro/outsourcing',], 3, 0, 'Backend', 'https://sibdev.pro/backend'
    ],

]

// Массив с данными страниц последних страниц

export let dataFinalPagesArray = [

    [

        "О нас", // Имя
        'https://sibdev.pro/studio', // URL
        1, // Иерархия
        'https://sibdev.pro', // Костыль 1
        'https://sibdev.pro/studio', // Костыль 2
        4, // Число отзывов
        '/services', // Ссылка нижней кнопки
    ],

    [

        "Вакансии", // Имя
        'https://sibdev.pro/vacancies', // URL
        1, // Иерархия
        '/', // Костыль 1
        'https://sibdev.pro/vacancies', // Костыль 2
         8, // Число вакансий
         5, // Число точек под слайдером
    
    ],

    [

        "Контакты", // Имя
        'https://sibdev.pro/contacts', // URL
        1, // Иерархия
        '/', // Костыль 1
        '/contacts', // Костыль 2
        '+7 (902) 924-44-51', // Телефон
        '@SibdevPro', // Телеграмм
        'a@sibdev.pro', // Почта
        'Sibdev', // Скайп
    
    ],

]