import {pagesArray} from './../check_data/main_elements'

describe('Проверка на отсутствие циклических ссылок', () => {
    for(let i of pagesArray) {
        if(i.name == 'Главная' || i.name == 'Практика' || i.name == 'Блог') continue;

        it(`Проврка страницы ${i.name}`, () => {
            cy.visit({
                url: i.url, 
                auth: {
                    username: "sibdev",
                    password: "zaq123wsxcde"
                }
            })

            cy.get('.breadcrumb .breadcrumb__content').within(() => {
                cy.get('a:last').should('not.have.attr', 'href')
                cy.get('a').then(($link) => {
                    for (let j = 0; j < $link.length - 1; j++) {
                        cy.get('a').eq(j).should('have.attr', 'href')
                    }
                })
            })

            cy.get('.breadcrumb .breadcrumb__content').within(() => {
                cy.get('a').should('not.have.attr', 'href', i.url)
            })
        })
    }
})