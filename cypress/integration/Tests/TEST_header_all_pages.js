import {pagesArray} from './../check_data/main_elements'

describe('Тестим хэдеры на всех страницах.\n Проверяем лого, кнопки и наличие в них правильных ссылок', () => {
    for(let i of pagesArray){
        if(i.name == 'Практика') continue;
        describe(`Тестим хэдер на странице ${i.name}`, () => {
            it('Заходим на страницу', () => {
                cy.visit(i.url)
            })

            if(i.name != 'Главная'){
                it('Тест ссылки ЛОГО страницы', () => {
                cy.get('header .header__logo').should('have.attr', 'href', 'https://sibdev.pro')
            })} else {
                it('Тест ссылки ЛОГО страницы', () => {
                    cy.get('header .header__logo').should('not.have.attr', 'href')
                })
            }

            it('Тест кнопки Услуги', () => {
                cy.get('header').contains('Услуги').should('have.attr', 'href', '/services')
            })

            it('Тест кнопок внутри Услуг', () => {
                cy.get('header').contains('Услуги').parent().within(() => {
                    cy.get('.drop-menu a').contains('Сайты').should('have.attr', 'href', '/site')
                    cy.get('.drop-menu a').contains('Лендинги').should('have.attr', 'href', '/landing')
                    cy.get('.drop-menu a').contains('Корпоративные сайты').should('have.attr', 'href', '/corporate-site')
                    cy.get('.drop-menu a').contains('Интернет-магазины').should('have.attr', 'href', '/online-store')
                    cy.get('.drop-menu a').contains('Сайты-визитки').should('have.attr', 'href', '/card-site')
                    cy.get('.drop-menu a').contains('Адаптивные сайты').should('have.attr', 'href', '/adaptive-site')
                    cy.get('.drop-menu a').contains('Веб-приложения (SPA)').should('have.attr', 'href', '/spa')
                    cy.get('.drop-menu a').contains('Чат-боты').should('have.attr', 'href', '/chat-bot')
                    cy.get('.drop-menu a').contains('Мобильные приложения').should('have.attr', 'href', '/mobile')
                    cy.get('.drop-menu a').contains('Утилиты, скрипты').should('have.attr', 'href', '/script')
                    cy.get('.drop-menu a').contains('Веб-сервисы').should('have.attr', 'href', '/saas')
                    cy.get('.drop-menu a').contains('Высоконагруженные сервисы').should('have.attr', 'href', '/highload')
                    cy.get('.drop-menu a').contains('Интернет-аукционы').should('have.attr', 'href', '/online-auction')
                    cy.get('.drop-menu a').contains('Тендерные площадки').should('have.attr', 'href', '/tender-place')
                    cy.get('.drop-menu a').contains('Агрегаторы').should('have.attr', 'href', '/agregator')
                    cy.get('.drop-menu a').contains('ИТ-аутсорс').should('have.attr', 'href', '/outstaffing')
                    cy.get('.drop-menu a').contains('Вёрстка').should('have.attr', 'href', '/markup')
                    cy.get('.drop-menu a').contains('Frontend').should('have.attr', 'href', '/frontend')
                    cy.get('.drop-menu a').contains('Backend').should('have.attr', 'href', '/backend')
                    cy.get('.drop-menu a').contains('Дизайн').should('have.attr', 'href', '/design')
                    cy.get('.drop-menu a').contains('Сопровождение').should('have.attr', 'href', '/maintenance')
                    cy.get('.drop-menu a').contains('Приложения').should('not.have.attr', 'href')
                })
            })

            it('Тест кнопки Портфолио', () => {
                cy.get('header').contains('Портфолио').should('have.attr', 'href', '/portfolio/')
            })

            it('Тест кнопки Блог', () => {
                cy.get('header').contains('Блог').should('have.attr', 'href', '/blog/')
            })

            it('Тест кнопки О нас', () => {
                cy.get('header').contains('О нас').should('have.attr', 'href', '/studio')
            })

            it('Тест кнопок внутри "О нас"', () => {
                cy.get('header').contains('О нас').parent().within(() => {
                    cy.get('.drop-menu a').contains('Студия').should('have.attr', 'href', '/studio')
                    cy.get('.drop-menu a').contains('Вакансии').should('have.attr', 'href', '/vacancies')
                    cy.get('.drop-menu a').contains('Практика').should('have.attr', 'href', '/practice/')
                })
            })

            it('Тест кнопки Контакты', () => {
                cy.get('header').contains('Контакты').should('have.attr', 'href', '/contacts')
            })

            it('Проверяем корректность ссылки с телефоном', () => {
                cy.get('header a.header__phone').should('have.attr', 'href', 'tel:8 (800) 201-97-32').should('have.attr', 'onclick', "ym(46083624,'reachGoal','Сlick_phone'); return true;")
            })
        })
    }
})