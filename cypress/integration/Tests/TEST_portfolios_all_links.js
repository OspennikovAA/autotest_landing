let portfolioPages = 20

describe('Тест всех портфолио', ()=> {

    for (let i = 0; i < portfolioPages; i++) {
        it(`Проверка кейса ${i}`, ()=> {
            cy.goToPage('http://sibdev.pro/portfolio/')
            cy.get('button.portfolio-work__button').click()
            cy.get('button.portfolio-work__button').click()

            cy.get('.portfolio__works a[href*="portfolio/case"]').eq(i).click()

            cy.get('a[href*="sibdev.digital"]').should('have.length', 0)
        })
    }

})






