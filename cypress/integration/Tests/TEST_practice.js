import {practice, questionArray} from './../check_data/main_elements'

describe('Полный тест страницы Практика', () => {

    describe('Тест шапки Практики', () => {

        it('Заходим на сайт', () => {
            cy.visit({
                url: practice, 
                auth: {
                    username: "sibdev",
                    password: "zaq123wsxcde"
                    }
            })
        
        })
    
        it('Проверяем Лого', () => {
            cy.get('a[class="header__logo"]:eq(0)').should('have.attr', 'href', '/')    
        })

        it('Тест ссылок хэдера при неавторизованном пользователе', () => {

            for(let i = 0; i <= 3; i++) {
    
                cy.get('.menu__body').within( () => {
    
                    cy.get('a').eq(i).click()
                    cy.url().should('include', '/login')

                    cy.visit({
                        url: practice, 
                        auth: {
                            username: "sibdev",
                            password: "zaq123wsxcde"
                            }
                    })
    
                })
    
            }
    
        })

    })

    describe('Тесты всяких кнопочек и т.д.', () => {
    
        it('Тест кнопки Регистрация', () => {
            cy.contains('a', 'Регистрация').click({force:true})
            cy.url().should('include', '#registration')    
        })
    
        it('Тест выпадающих вопросов', () => {
    
            for (let i = 0; i < questionArray.length; i++) {
    
                cy.get('.faq__item').eq(i).within(() => {    
                    cy.contains(questionArray[i]).should('not.be.visible')
                    cy.get('.faq__item-question').click({force:true})
                    cy.contains(questionArray[i]).should('be.visible')
                    cy.get('.faq__item-dropdown-icon').click({force:true})
                    cy.contains(questionArray[i]).should('not.visible')    
                })
    
            }
    
        })
    
        it('Тест кнопочек карусели выпускников', () => {
    
            cy.get('.reviews__body').within( () => {
                for (let i = 0; i < 4; i++) {
                    cy.get('#reviewsSlider__left').click({force:true}).wait(200)
                }

                for (let i = 0; i < 4; i++) {
                    cy.get('#reviewsSlider__right').click({force:true}).wait(200)
                }

                for (let i = 0; i < 6; i++) {
                    cy.get('.lSPager a').eq(i%2).click({force:true}).wait(200)
                }
            })
    
        })
    
        it('Проверка правильности ссылки Телеграмм', () => {    
            cy.get('.footer__content a').should('have.attr', 'href', 'https://t.me/SibdevHR')    
        })
    
        it('Проверка работоспособности выбора технологии и формы', () => {
    
            cy.get('.form__specialization').within( () => {    
                cy.get('input').eq(0).click({force:true})
                cy.get('input').eq(1).click({force:true})
                cy.get('input').eq(2).click({force:true})    
            })
    
            // cy.get('input[name="full_name"]')
            //   .type('Тестовое имя')
            //   .should('have.value', 'Тестовое имя')
    
            // cy.get('input[name="phone"]')
            //   .type('9509915334')
            //   .should('have.value', '9509915334')
    
        })
    
    
    })

})