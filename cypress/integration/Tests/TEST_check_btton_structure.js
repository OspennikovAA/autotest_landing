import {pagesArray} from './../check_data/main_elements'

describe('Проверка на отсутствие циклических ссылок', () => {
    for(let i = 0; i < pagesArray.length; i++) {
        if((i != 1)&&(i != 5)&&(i!=28)) {
            it(`Проврка кнопок страницы ${pagesArray[i][0]}`, () => {
                cy.visit( pagesArray[i][1])

                if(i==0) {
                    cy.get('button.common-button:first').should('contain', 'Оставить заявку')
                    cy.get('a.common-button').should('contain', 'В портфолио')
                    cy.get('button.common-button:last').should('contain', 'Оставить заявку')
                }
                
                if(i==2) {
                    cy.get('button.about__button:first').should('contain', 'Связаться с нами')
                    cy.get('button.portfolio-work__button:last').should('contain', 'Показать ещё')
                }
                
                if(i==3) {
                    cy.get('a.button').should('contain', 'Перейти к услугам')
                }
                
                if(i==4) {
                    cy.get('button.about__button').should('contain', 'Отправить заявку')
                }

                if(i>=6 && i <=27) {
                    cy.get('button.about__button:first').should('have.value', 'Отправить заявку')
                    cy.get('button.why-us__button:last').should('have.value', 'Отправить заявку')
                }

            })
        }
    }
})