import {mainPage, itemArray} from './../check_data/main_elements'

describe('Генеральный тест', () => {

  it('Заходим на сайт', () => {cy.visit({
    url: mainPage
    })

  })

  describe('Тест модуля авторизации блок С нами комфортно', () => {

    it('Проверка работоспособности кнопки в блоке "С нами комфортно"', () => {

      cy.clickButton('comfort', 'Оставить заявку')

      cy.contains('Кратко опишите идею вашего проекта').should('be.visible')

      cy.clickCross('Оставить заявку').wait(1000)

    })

  })

  describe('Тест оставления заявки с ошибками', () => {

    it('Ошибка если сообщения нет', () => {  
      // Ошибка - не заполнено сообщение  
      cy.clickButton('startScreen', 'Оставить заявку')

      cy.typeInTa('Ваше имя', 'Name')
      cy.typeInTa('Ваш e-mail', 'test@sibdev.com')

      cy.clickButton('modal', 'Отправить').wait(200)

      cy.contains('Некоторые из полей заполнены неверно.').should('be.visible')

      cy.clickCross('Ошибка').wait(200)
      cy.clearAll('modal')
      cy.clickCross('Оставить заявку').wait(1000)
    })

    it('Ошибка если имени нет', () => {  
      // Не заполнено Имя  
      cy.clickButton('startScreen', 'Оставить заявку')

      cy.typeInTa('Сообщение', 'Тестовое сообщение')
      cy.typeInTa('Ваш e-mail', 'test@sibdev.com')

      cy.clickButton('modal', 'Отправить').wait(200)

      cy.contains('Некоторые из полей заполнены неверно.').should('be.visible')

      cy.clickCross('Ошибка')
      cy.clearAll('modal')
      cy.clickCross('Оставить заявку').wait(1000)
    })

    it('Ошибка если имя и email перепутаны', () => {  
      // Имя и email перепутаны  
      cy.clickButton('startScreen', 'Оставить заявку')

      cy.typeInTa('Сообщение', 'Тестовое сообщение')
      cy.typeInTa('Ваш e-mail', 'Name')
      cy.typeInTa('Ваше имя', 'test@sibdev.com')

      cy.clickButton('modal', 'Отправить').wait(200)

      cy.contains('Некоторые из полей заполнены неверно.').should('be.visible')

      cy.clickCross('Ошибка')
      cy.clearAll('modal')
      cy.clickCross('Оставить заявку').wait(1000)
    })

    it('Ошибка если email нет', () => {  
      // Не заполнен email  
      cy.clickButton('startScreen', 'Оставить заявку')

      cy.typeInTa('Сообщение', 'Тестовое сообщение')
      cy.typeInTa('Ваше имя', 'Name')

      cy.clickButton('modal', 'Отправить').wait(200)

      cy.contains('Некоторые из полей заполнены неверно.').should('be.visible')

      cy.clickCross('Ошибка')
      cy.clearAll('modal')
      cy.clickCross('Оставить заявку').wait(1000)
    })

    it('Ошибка если email некорректен', () => {      
      // Неверный email пока тоько email без @, ИЗВЕСТНЫЙ БАГ - отправка email без домена  
      cy.clickButton('startScreen', 'Оставить заявку')

      cy.typeInTa('Сообщение', 'Тестовое сообщение')
      cy.typeInTa('Ваш e-mail', 'testsibdev.com')
      cy.typeInTa('Ваше имя', 'Name')

      cy.clickButton('modal', 'Отправить').wait(200)

      cy.contains('Некоторые из полей заполнены неверно.').should('be.visible')

      cy.clickCross('Ошибка')
      cy.clearAll('modal')
      cy.clickCross('Оставить заявку').wait(1000)  
    })

      
  })

  describe('Тест блока Портфолио', () => {

    it('Проверяем ссылку на кейс', () => {
      cy.contains('a', 'СМОТРЕТЬ КЕЙС').should('have.attr', 'href', '/portfolio/case/liga-online-cyberfootball-tournaments-service/')
    })

    it('Проверка кнопки "В портфолио', () => {
      cy.contains('a','В портфолио').should('have.attr', 'href', '/portfolio/')
    })

  })

  describe('Тест блока Команда', () => {
  
    it('Тест правой стрелочки карусели', () => {
      for (let i = 0; i < 4; i++) {
        cy.get('.hooper-next').click().wait(500)
      }
    })

    it('Тест левой стрелочки карусели', () => {
      for (let i = 0; i < 4; i++) {
        cy.get('.hooper-prev').click().wait(500)
      }
  })

  })

  describe('Тест блока Отзывы', () => {

    it('Тест кликабельности грамот', () => {

      cy.get('.feedback-item').then(($num) => {
        for(let i = 0; i < $num.length; i++) {
          cy.get('.feedback-item__image').eq(i).click()
        }
      })

    })

    it('Тест кликабельности ссылок рядом с грамотами', () => {

      cy.get('.feedback-item').then(($num) => {
        for(let i = 0; i < $num.length; i++) {
          cy.get('.feedback-item__company a').eq(i).click()
        }
      })

    })

  })

})
