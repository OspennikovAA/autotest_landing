import {pagesArray} from './../check_data/main_elements'

describe('Тестим футеры на всех страницах.', () => {
    for(let i of pagesArray){
        if(i.name == 'Практика') continue;
        
        describe(`Тестим футер на странице ${i.name}`, () => {
            it('Заходим на страницу', () => {
                cy.visit(i.url)
            })

            if(i.name != 'Главная'){
                it('Тест ссылки ЛОГО страницы', () => {
                cy.get('footer .footer-logo').should('have.attr', 'href', 'https://sibdev.pro')
            })} else {
                it('Тест ссылки ЛОГО страницы', () => {
                    cy.get('footer .footer-logo').should('not.have.attr', 'href')
                })
            }

            it('Тест колонки Сайты', () => {
                cy.get('footer').contains('Сайты').should('have.attr', 'href', '/site')
            })

            it('Тест кнопок внутри Сайты', () => {
                cy.get('footer').contains('Сайты').parent().within(() => {
                    cy.get('nav a').contains('Лендинги').should('have.attr', 'href', '/landing')
                    cy.get('nav a').contains('Корпоративные сайты').should('have.attr', 'href', '/corporate-site')
                    cy.get('nav a').contains('Интернет-магазины').should('have.attr', 'href', '/online-store')
                    cy.get('nav a').contains('Сайты-визитки').should('have.attr', 'href', '/card-site')
                    cy.get('nav a').contains('Адаптивные сайты').should('have.attr', 'href', '/adaptive-site')
                })
            })

            it('Тест колонки ИТ-аутсорс', () => {
                cy.get('footer').contains('ИТ-аутсорс').should('have.attr', 'href', '/outstaffing')
            })

            it('Тест кнопок внутри ИТ-аутсорс', () => {
                cy.get('footer').contains('ИТ-аутсорс').parent().within(() => {
                    cy.get('nav a').contains('Вёрстка').should('have.attr', 'href', '/markup')
                    cy.get('nav a').contains('Frontend').should('have.attr', 'href', '/frontend')
                    cy.get('nav a').contains('Backend').should('have.attr', 'href', '/backend')
                })
            })

            it('Тест колонки Веб-сервисы', () => {
                cy.get('footer').contains('Веб-сервисы').should('have.attr', 'href', '/saas')
            })

            it('Тест кнопок внутри Веб-сервисы', () => {
                cy.get('footer').contains('Веб-сервисы').parent().within(() => {
                    cy.get('nav a').contains('Высоконагруженные сервисы').should('have.attr', 'href', '/highload')
                    cy.get('nav a').contains('Интернет-аукционы').should('have.attr', 'href', '/online-auction')
                    cy.get('nav a').contains('Тендерные площадки').should('have.attr', 'href', '/tender-place')
                    cy.get('nav a').contains('Агрегаторы').should('have.attr', 'href', '/agregator')
                })
            })

            it('Тест колонки Приложения', () => {
                cy.get('footer').contains('Приложения').should('not.have.attr', 'href')
            })

            it('Тест кнопок внутри Приложения', () => {
                cy.get('footer').contains('Приложения').parent().within(() => {
                    cy.get('nav a').contains('Чат-боты').should('have.attr', 'href', '/chat-bot')
                    cy.get('nav a').contains('Мобильные приложения').should('have.attr', 'href', '/mobile')
                    cy.get('nav a').contains('Утилиты, скрипты').should('have.attr', 'href', '/script')
                    cy.get('nav a').contains('Веб-приложения (SPA)').should('have.attr', 'href', '/spa')
                })
            })

            it('Тест колонки О нас', () => {
                cy.get('footer').contains('О нас').should('have.attr', 'href', '/studio')
            })

            it('Тест кнопок внутри "О нас"', () => {
                cy.get('footer').contains('О нас').parent().within(() => {
                    cy.get('.footer-grid__cell--about-us a').contains('Студия').should('have.attr', 'href', '/studio')
                    cy.get('.footer-grid__cell--about-us a').contains('Вакансии').should('have.attr', 'href', '/vacancies')
                    cy.get('.footer-grid__cell--about-us a').contains('Портфолио').should('have.attr', 'href', '/portfolio/')
                    cy.get('.footer-grid__cell--about-us a').contains('Блог').should('have.attr', 'href', '/blog/')
                })
            })

            it('Тест кнопки Дизайн', () => {
                cy.get('footer a').contains('Дизайн').should('have.attr', 'href', '/design')
            })

            it('Тест кнопки Сопровождение', () => {
                cy.get('footer a').contains('Сопровождение').should('have.attr', 'href', '/maintenance')
            })

            it('Проверка блока "Свяжитесь с нами"', () => {
                cy.get('footer a').contains('Cвяжитесь с нами').should('not.have.attr', 'href')

                cy.get('footer a').contains('Cвяжитесь с нами').parent().within(() => {
                    cy.get('a.footer__phone').should('have.attr', 'href', 'tel:8 (800) 201-97-32').should('have.attr', 'onclick', "ym(46083624,'reachGoal','Сlick_phone'); return true;")
                    cy.get('.footer-contact a').contains('a@sibdev.pro').should('have.attr', 'href', 'mailto:a@sibdev.pro')
                    cy.get('.footer-contact a').contains('@SibdevPro').should('have.attr', 'href', 'https://telegram.me/sibdevpro')
                    cy.get('.footer-contact a[itemprop="address"]').should('not.have.attr', 'href')

                    cy.get('.footer-contact span').contains('e-mail')
                    cy.get('.footer-contact span').contains('telegram')
                    cy.get('.footer-contact span').contains('skype')
                    cy.get('.footer-contact span').contains('адрес')
                })
            })

            it('Проверка ссылок на соцсети', () => {
                cy.get('footer a').contains('Следите за нами').parent().within(() => {
                    cy.get('a i.fa-facebook-f').parent().should('have.attr', 'href', 'https://www.facebook.com/sibdevstudio/')
                    cy.get('a i.fa-vk').parent().should('have.attr', 'href', 'https://vk.com/sibdevpro')
                    cy.get('a i.fa-instagram').parent().should('have.attr', 'href', 'https://www.instagram.com/sibdevstudio')
                    cy.get('a i.fa-medium').parent().should('have.attr', 'href', 'https://medium.com/sibdev')
                })
            })
        })
    }
})