import {dataFinalPagesArray, itemArray} from './../check_data/main_elements'

describe('Полный тест последних страниц', () => {

    for (let page = 0; page < dataFinalPagesArray.length; page++) {

        describe('Тест страницы ' + dataFinalPagesArray[page][0], () => {
        
            it('Заходим на страницу', () => {cy.visit({
                    url: dataFinalPagesArray[page][1], 
                    auth: {
                    username: "sibdev",
                    password: "zaq123wsxcde"
                    }

                })
            })

            // !!!!!!!!!!!!!!!!!!
            // Тело тестов Хэдера

            describe('Тест Хэдера', () => {

                it('Тест ссылки ЛОГО страницы', () => {
                    cy.get('header .header__logo').should('have.attr', 'href', '/')
                })
            
                it('Тест кнопки Услуги', () => {
                    cy.get('header').contains('Услуги').should('have.attr', 'href', '/services')
                })
            
                it('Тест кнопки Портфолио', () => {
                    cy.get('header').contains('Портфолио').should('have.attr', 'href', '/portfolio/')
                })
            
                it('Тест кнопки О нас', () => {
                    cy.get('header').contains('О нас').should('have.attr', 'href', '/studio')
                })
            
                it('Тест кнопки Контакты', () => {
            
                    cy.get('header').contains('Контакты').should('have.attr', 'href', '/contacts')
                })
                
              })

            // !!!!!!!!!!!!!!!!!! 
            // Тело тестов Футера

            describe('Тест Футера страницы', () => {

                it('Тык на лого футера', () => {
                    cy.get('footer .footer__logo').should('have.attr', 'href', '/')  
                })
            
                it('Тест кнопки Контакты в Футере', () => {
                    cy.get('footer').contains('Контакты').should('have.attr', 'href', '/contacts')
                })
            
                it('Тест ссылок в двойном цикле с 3-мерным массивом', () => {
                  for(let i = 0; i < itemArray.length; i++) {
            
                    for(let t = 0; t < itemArray[i][0].length; t++) {  
                      cy.get('.footer__column').eq(1).within( () => {
                          cy.get('.footer__item').eq(i).within( () => {
                            cy.get('.column__title a').eq(t).should('have.attr', 'href', itemArray[i][0][t])
                          })
                        })  
                      }
              
                    for(let l = 0; l < itemArray[i][1].length; l++) {  
                      cy.get('.footer__column').eq(1).within( () => {
                        cy.get('.footer__item').eq(i).within( () => {
                        cy.get('.column__links a').eq(l).should('have.attr', 'href', itemArray[i][1][l])
                        })
                      })  
                    }
              
                  }
              
                })
            
                it('Тест ссылок на соцсети', () => {
            
                  cy.get('.footer-end__icons').within( () =>{
                      cy.get('a:eq(0)').should('have.attr', 'href', 'https://medium.com/sibdev')
                      cy.get('a:eq(1)').should('have.attr', 'href', 'https://vk.com/sibdevpro')
                      cy.get('a:eq(2)').should('have.attr', 'href', 'https://twitter.com/SibdevStudio')
                      cy.get('a:eq(3)').should('have.attr', 'href', 'https://www.instagram.com/sibdevstudio')
                  })
            
                })
            
              })

            // !!!!!!!!!!!!!!!!!!
            // Тело тестов Тела страницы

            describe('Тест тела страницы ' + dataFinalPagesArray[page][0], () => {

                it('Тест хлебных крошек', () => {

                    cy.get('.breadcrumb__content').within( () => {
                        cy.contains(dataFinalPagesArray[page][0]).should('be.visible')
                        cy.contains(dataFinalPagesArray[page][0]).should('have.attr', 'href', dataFinalPagesArray[page][4])            
                    })
                    cy.get('.breadcrumb__content').within( () => {
                        cy.get('a').eq(0).should('have.attr', 'href', dataFinalPagesArray[page][3])
                    })

                })

                if ( dataFinalPagesArray[page][0] == "О нас" ) {

                    it('Проверка числа отзывов', () => {
                        cy.get('.feedback-item').should('have.length', dataFinalPagesArray[page][5])
                    })

                    for ( let f = 0; f <  dataFinalPagesArray[page][5]; f++) {

                        it('Проверка видимости и ссылки грамоты ' + (f+1), () => {
                            cy.get('.feedback-item__image').eq(0).should('be.visible')

                            cy.get('.feedback-item__image').within(() => {
                                cy.get('a[href*="media/landing/img/letter"]').should('have.attr', 'target', '_blank')
                            })
                        })

                    }

                    for ( let f = 0; f <  dataFinalPagesArray[page][5]; f++) {

                        it(`Проверка ссылки ${f+1} в отзывах`, () => {
                            cy.get('.feedback-item__company a').eq(f).should('have.attr', 'href')
                        })

                    }

                    it('Проверка кнопки Перейти к услугам', () => {
                        cy.contains('a', 'Перейти к услугам').should('have.attr', 'href', dataFinalPagesArray[page][6])
                    })

                }

                if ( dataFinalPagesArray[page][0] == "Вакансии" ) {

                    describe('Тест модального окна', () => {

                        it('Тест кнопки Присоединяйся к нам', () => {
                            cy.clickButton('joinUs', 'Присоединяйся к нам').wait(300)
                            cy.contains('Отправь резюме').should('be.visible')
                            cy.get('.vacancy-modal__close-btn').click().wait(300)
                            cy.contains('Отправь резюме').should('not.be.visible')
                        })

                    })

                    describe('Проверка списка вакансий', () => {

                        it('Проверка числа вакансий', () => {
                            cy.get('.vacancy').should('have.length', dataFinalPagesArray[page][5])    
                        })
    
                        for ( let v = 0; v < dataFinalPagesArray[page][5]; v++) {

                            it('Проверка выпадающего меню вакансии' + (v+1), () => {                                
                                cy.get('div[class="vacancy__body"]').eq(v).should('not.be.visible')
                                cy.get('div[class="vacancy__toggle-angle"]').eq(v).should('have.attr', 'class', 'vacancy__toggle-angle')
                                cy.get('.vacancy__form').should('not.be.visible')

                                cy.get('.vacancy').eq(v).click()

                                
                                cy.get('.vacancy__body').eq(v).should('be.visible')
                                cy.get('.vacancy__toggle-angle').eq(v).should('have.attr', 'class', 'vacancy__toggle-angle rotate180')
                                cy.get('.vacancy__form').should('be.visible')


                                cy.get('.vacancy').eq(v).click('top')

                                
                                cy.get('.vacancy__body').eq(v).should('not.be.visible')
                                cy.get('.vacancy__toggle-angle').eq(v).should('have.attr', 'class', 'vacancy__toggle-angle')
                                cy.get('.vacancy__form').should('not.be.visible')
                            })

                        }

                    })

                    describe('Тест кнопочек слайдера', () => {

                        for ( let sb = 0; sb < dataFinalPagesArray[page][6]; sb++) {

                            it('Тык на кнопочку', () => {
                                cy.get('.lSPager li').eq(sb).click()
                                cy.get('.lSPager li').eq(sb).should('have.attr', 'class', 'active')
                            })

                        }

                    })

                }

                if ( dataFinalPagesArray[page][0] == "Контакты") {

                    describe('Тест модального окна', () => {


                        it('Тест кнопки Отправить заявку', () => {
                            cy.clickButton('about', 'Отправить заявку').wait(300)
                            cy.contains('Кратко опишите идею вашего проекта').should('be.visible')
                            cy.get('.modal__close-application').click().wait(300)
                            cy.contains('Кратко опишите идею вашего проекта').should('not.be.visible')
                        })

                    })

                    describe('Проверка правильности контактных данных', () => {

                        it('Проверка Телефона', () => {
                            cy.get('.kontakt__list').within(() => {
                                cy.contains(dataFinalPagesArray[page][5]).should('be.visible')
                            })
                        })

                        it('Проверка Телеграмма', () => {
                            cy.get('.kontakt__list').contains(dataFinalPagesArray[page][6]).should('be.visible')
                        })

                        it('Проверка почты', () => {
                            cy.get('.kontakt__list').contains(dataFinalPagesArray[page][7]).should('be.visible')
                        })

                        it('Проверка скайпа', () => {
                            cy.get('.kontakt__list').contains(dataFinalPagesArray[page][8]).should('be.visible')
                        })

                    })

                }               

            })

        })

    }

})