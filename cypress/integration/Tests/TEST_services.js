import {servicesPage, bodyLinksArray, itemArray} from './../check_data/main_elements'

describe('Общий тест страницы Услуги', () => {

    it('Заходим на сайт', () => {cy.visit({
        url: servicesPage, 
        auth: {
            username: "sibdev",
            password: "zaq123wsxcde"
            }
        })
    
    })
    
    describe('Тест Хэдера', () => {

        it('Тест ссылки ЛОГО страницы', () => {
            cy.get('header .header__logo').should('have.attr', 'href', '/')
        })
    
        it('Тест кнопки Услуги', () => {
            cy.get('header').contains('Услуги').should('have.attr', 'href', '/services')
        })
    
        it('Тест кнопки Портфолио', () => {
            cy.get('header').contains('Портфолио').should('have.attr', 'href', '/portfolio/')
        })
    
        it('Тест кнопки О нас', () => {
            cy.get('header').contains('О нас').should('have.attr', 'href', '/studio')
        })
    
        it('Тест кнопки Контакты', () => {
    
            cy.get('header').contains('Контакты').should('have.attr', 'href', '/contacts')
        })
        
    })

    describe('Тест Футера страницы', () => {

        it('Тык на лого футера', () => {
            cy.get('footer .footer__logo').should('have.attr', 'href', '/')  
        })
    
        it('Тест кнопки Контакты в Футере', () => {
            cy.get('footer').contains('Контакты').should('have.attr', 'href', '/contacts')
        })
    
        it('Тест ссылок в двойном цикле с 3-мерным массивом', () => {
          for(let i = 0; i < itemArray.length; i++) {
    
            for(let t = 0; t < itemArray[i][0].length; t++) {  
              cy.get('.footer__column').eq(1).within( () => {
                  cy.get('.footer__item').eq(i).within( () => {
                    cy.get('.column__title a').eq(t).should('have.attr', 'href', itemArray[i][0][t])
                  })
                })  
              }
      
            for(let l = 0; l < itemArray[i][1].length; l++) {  
              cy.get('.footer__column').eq(1).within( () => {
                cy.get('.footer__item').eq(i).within( () => {
                cy.get('.column__links a').eq(l).should('have.attr', 'href', itemArray[i][1][l])
                })
              })  
            }
      
          }
      
        })
    
        it('Тест ссылок на соцсети', () => {
    
          cy.get('.footer-end__icons').within( () =>{
              cy.get('a:eq(0)').should('have.attr', 'href', 'https://medium.com/sibdev')
              cy.get('a:eq(1)').should('have.attr', 'href', 'https://vk.com/sibdevpro')
              cy.get('a:eq(2)').should('have.attr', 'href', 'https://twitter.com/SibdevStudio')
              cy.get('a:eq(3)').should('have.attr', 'href', 'https://www.instagram.com/sibdevstudio')
          })
    
        })
    
    })

    describe('Тест работоспособности ссылок на странице Услуги', () => {

        it('Проверка бредкрамбсов', () => {
            cy.get('.breadcrumb__content').contains('a','Услуги').should('have.attr', 'href', 'https://sibdev.pro/services')        
            cy.get('.breadcrumb__content').contains('a', 'Sibdev').should('have.attr', 'href', 'http://sibdev.pro/')
        })

        it('Тест ссылок тела Услуги в цикле с 2-мерным массивом', () => {
            for(let i = 0; i < bodyLinksArray.length; i++) {      
                for(let t = 0; t < bodyLinksArray[i].length; t++) {
      
                  cy.get('.uslugi__item').eq(i).within(() => {  
                    cy.get('a').eq(t).should('have.attr', 'href', bodyLinksArray[i][t])               
                  })
      
                }      
            }      
        })
    
    })

})