import {portfolioPage, itemArray} from './../check_data/main_elements'

describe('Генеральный тест', () => {

  it('Заходим на сайт', () => {cy.visit( portfolioPage)})

  describe('Тест Хэдера', () => {

    it('Тест ссылки ЛОГО страницы', () => {
        cy.get('header .header__logo').should('have.attr', 'href', '/')
    })

    it('Тест кнопки Услуги', () => {
        cy.get('header').contains('Услуги').should('have.attr', 'href', '/services')
    })

    it('Тест кнопки Портфолио', () => {
        cy.get('header').contains('Портфолио').should('have.attr', 'href', '/portfolio/')
    })

    it('Тест кнопки О нас', () => {
        cy.get('header').contains('О нас').should('have.attr', 'href', '/studio')
    })

    it('Тест кнопки Контакты', () => {

        cy.get('header').contains('Контакты').should('have.attr', 'href', '/contacts')
    })
    
  })

  describe('Тест Футера страницы', () => {

    it('Тык на лого футера', () => {
        cy.get('footer .footer__logo').should('have.attr', 'href', '/')  
    })

    it('Тест кнопки Контакты в Футере', () => {
        cy.get('footer').contains('Контакты').should('have.attr', 'href', '/contacts')
    })

    it('Тест ссылок в двойном цикле с 3-мерным массивом', () => {
      for(let i = 0; i < itemArray.length; i++) {

        for(let t = 0; t < itemArray[i][0].length; t++) {  
          cy.get('.footer__column').eq(1).within( () => {
              cy.get('.footer__item').eq(i).within( () => {
                cy.get('.column__title a').eq(t).should('have.attr', 'href', itemArray[i][0][t])
              })
            })  
          }
  
        for(let l = 0; l < itemArray[i][1].length; l++) {  
          cy.get('.footer__column').eq(1).within( () => {
            cy.get('.footer__item').eq(i).within( () => {
            cy.get('.column__links a').eq(l).should('have.attr', 'href', itemArray[i][1][l])
            })
          })  
        }
  
      }
  
    })

    it('Тест ссылок на соцсети', () => {

      cy.get('.footer-end__icons').within( () =>{
        cy.get('a:eq(0)').should('have.attr', 'href', 'https://medium.com/sibdev')
        cy.get('a:eq(1)').should('have.attr', 'href', 'https://vk.com/sibdevpro')
        cy.get('a:eq(2)').should('have.attr', 'href', 'https://twitter.com/SibdevStudio')
        cy.get('a:eq(3)').should('have.attr', 'href', 'https://www.instagram.com/sibdevstudio')
      })

    })

  })

  describe('Тест тела страницы', () => {

    it('Проверка бредкрамбсов', () => {
      cy.get('.breadcrumb__content').contains('a','Портфолио').should('not.have.attr', 'href')        
      cy.get('.breadcrumb__content').contains('a', 'Sibdev').should('have.attr', 'href', '/')
    })

    it('Проверка кнопки Связаться с нами', () => {
      cy.contains('button', 'Связаться с нами').click()
      cy.contains('Кратко опишите идею вашего проекта').should('be.visible')
      cy.clickCross('Оставить заявку')
    })


    it('Тест карточек и кнопки "Показать ещё" из основного контента Порфолио', () => {
      
      cy.get('.portfolio__works .work__item').should('have.length', 8)
      cy.contains('button', 'Показать ещё').click()
      cy.get('.portfolio__works .work__item').should('have.length', 16)
      cy.contains('button', 'Показать ещё').click()
      cy.get('.portfolio__works .work__item').should('have.length', 24)
      cy.contains('button', 'Показать ещё').click()
      cy.get('.portfolio__works .work__item').should('have.length', 27)
    })
      
    it('Тест кнопок переключения карточек кейсов', () => {
      for (let i = 4; i >= 0; i--) {
        cy.get('.portfolio__tabs .tab__item').eq(i)
        .click()
        .should('have.class', 'tab__item_active')
      }
    })

    it('Проверяем что ссылки на кейсы в принципе существуют', () => {
      cy.get('.work__item a[href*="case/"]').then(($num) => {
          expect($num.length).not.to.be.equal(0)
      })        
    })

    it('Проверяем что любой кейс содержит ХОТЯ БЫ 1 тэг', () => {
      cy.get('.work__item').then(($cas) => {
        for(let k = 0; k < $cas.length; k++) {
          cy.get('.portfolio__works .work__item').eq(k).within(() => {        
            cy.get('.work__tags a').should('have.length.of.at.least', 1)     
          })
        }
      })        
    })

  })
})