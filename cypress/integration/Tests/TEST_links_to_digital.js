import {pagesArray} from '../check_data/main_elements'

describe('Тест наличия хотя бы одной ссылки на pro', () => {

    for( let i = 0; i < pagesArray.length; i++) {

        describe('Тест страницы ' + pagesArray[i][0], () => {

            it('Заходим на страницу', () => {
                cy.goToPage(pagesArray[i][1])
            })

            it('Проверяем наличие ссылок', () => {
                cy.get('a[href*="sibdev.digital"]').should('have.length', 0)
            })
        })

    }

})