import {itemArray, dataPagesServicesArray} from './../check_data/main_elements'

describe('Полный тест всех страниц', () => {

    for (let page = 0; page < dataPagesServicesArray.length; page++) {

        describe('Тест страницы ' + dataPagesServicesArray[page][6], () => {
        
            it('Заходим на страницу', () => {cy.visit({
                url: dataPagesServicesArray[page][0], 
                auth: {
                    username: "sibdev",
                    password: "zaq123wsxcde"
                    }
                })
            })

            // !!!!!!!!!!!!!!!!!!
            // Тело тестов Хэдера

            describe('Тест Хэдера', () => {

                it('Тест ссылки ЛОГО страницы', () => {
                    cy.get('header .header__logo').should('have.attr', 'href', '/')
                })
            
                it('Тест кнопки Услуги', () => {
                    cy.get('header').contains('Услуги').should('have.attr', 'href', '/services')
                })
            
                it('Тест кнопки Портфолио', () => {
                    cy.get('header').contains('Портфолио').should('have.attr', 'href', '/portfolio/')
                })
            
                it('Тест кнопки О нас', () => {
                    cy.get('header').contains('О нас').should('have.attr', 'href', '/studio')
                })
            
                it('Тест кнопки Контакты', () => {
            
                    cy.get('header').contains('Контакты').should('have.attr', 'href', '/contacts')
                })
                
              })

            // !!!!!!!!!!!!!!!!!! 
            // Тело тестов Футера

            describe('Тест Футера страницы', () => {

                it('Тык на лого футера', () => {
                    cy.get('footer .footer__logo').should('have.attr', 'href', '/')  
                })
            
                it('Тест кнопки Контакты в Футере', () => {
                    cy.get('footer').contains('Контакты').should('have.attr', 'href', '/contacts')
                })
            
                it('Тест ссылок в двойном цикле с 3-мерным массивом', () => {
                  for(let i = 0; i < itemArray.length; i++) {
            
                    for(let t = 0; t < itemArray[i][0].length; t++) {  
                      cy.get('.footer__column').eq(1).within( () => {
                          cy.get('.footer__item').eq(i).within( () => {
                            cy.get('.column__title a').eq(t).should('have.attr', 'href', itemArray[i][0][t])
                          })
                        })  
                      }
              
                    for(let l = 0; l < itemArray[i][1].length; l++) {  
                      cy.get('.footer__column').eq(1).within( () => {
                        cy.get('.footer__item').eq(i).within( () => {
                        cy.get('.column__links a').eq(l).should('have.attr', 'href', itemArray[i][1][l])
                        })
                      })  
                    }
              
                  }
              
                })
            
                it('Тест ссылок на соцсети', () => {
            
                  cy.get('.footer-end__icons').within( () =>{
                      cy.get('a:eq(0)').should('have.attr', 'href', 'https://medium.com/sibdev')
                      cy.get('a:eq(1)').should('have.attr', 'href', 'https://vk.com/sibdevpro')
                      cy.get('a:eq(2)').should('have.attr', 'href', 'https://twitter.com/SibdevStudio')
                      cy.get('a:eq(3)').should('have.attr', 'href', 'https://www.instagram.com/sibdevstudio')
                  })
            
                })
            
              })

            // !!!!!!!!!!!!!!!!!!
            // Тело тестов Тела страницы

            describe('Тест тела страницы ' + dataPagesServicesArray[page][6], () => {

                it('Тест хлебных крошек', () => {

                    cy.get('.breadcrumb__content').within( () => {            
                        cy.contains(dataPagesServicesArray[page][6]).should('be.visible')
                        cy.contains(dataPagesServicesArray[page][6]).should('have.attr', 'href', dataPagesServicesArray[page][7])            
                    })

                    for(let bread = 0; bread < dataPagesServicesArray[page][2] - 1; bread++) {
                        cy.get('.breadcrumb__content').within( () => {
                            cy.get('a').eq(bread).should('have.attr', 'href', dataPagesServicesArray[page][2][bread])
                        })
                    }

                })

                
            for(let bread = 0; bread < (dataPagesServicesArray[page][2].length); bread++) {

                it('Тест родительских хлебных крошек', () => {
                    cy.get('.breadcrumb__content').within(() => {
                        cy.get('a').eq(bread).should('have.attr', 'href', dataPagesServicesArray[page][2][bread])
                    })
                })

            }

                it('Тест верхнего модального окна', () => {
                    cy.contains('Кратко опишите идею вашего проекта').should('not.be.visible')
                    cy.clickButton('about', 'Оставить заявку').wait(300)
                    cy.contains('Кратко опишите идею вашего проекта').should('be.visible')
                    cy.clickCross('Оставить заявку').wait(300)
                    cy.contains('Кратко опишите идею вашего проекта').should('not.be.visible')
                })

                it('Тест нижнего модального окна', () => {
                    cy.contains('Кратко опишите идею вашего проекта').should('not.be.visible')
                    cy.clickButton('whyUs', 'Оставить заявку').wait(300)
                    cy.contains('Кратко опишите идею вашего проекта').should('be.visible')
                    cy.clickCross('Оставить заявку').wait(300)
                    cy.contains('Кратко опишите идею вашего проекта').should('not.be.visible')        
                })


                if( dataPagesServicesArray[page][3] > 2) {

                    it('Тест кнопки развернуть',() => {
                        cy.contains('a', 'Развернуть').click()
                        cy.get('.information__text p').eq(dataPagesServicesArray[page][3] - 1).should('be.visible')
                    })                    

                }

                if( page !== 12 ) {
                    it('Проверка ссылок на кейсы', () => {
                        cy.get('.portfolio__work a[href*="/case/"]').should('have.length', dataPagesServicesArray[page][5])
                    })
                }

            })

        })

    }

})