// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

// Данная команда будет нажимать кнопочку в интересующем нас блоке
// Ну вроде Оставить заявку, Показать ещё и т.д.
// Команда принимает атрибуты:
// block - интересующий нас блок !!! Поиск нужного блока будет осуществляться другой командой, работающей через объект с методами !!!
// txt - текст внутри кнопочки
// ТЕОРЕТИЧЕСКИ индекс кнопочки указывать не придётся, ибо в каждом блоке есть не более 1 кнопочки.
// ТЕОРЕТИЧЕСКИ не важно реализована кнопка через div или button, этот тэг будет родительским относительно того, в котором лежит текст кнопки
// Если это не так, вводим параметр index и ветвим команду для разных случаев
Cypress.Commands.add('clickButton', (block, txt) => {
    cy.getBlock(block).within(() => {
        cy.contains(txt).click({force: true}) // ПРЕДПОЛОЖЕНИЕ - текст кнопки лежит в дочернем тэге, а нам нужен родительский
    })
})

// Данная команда выполняет поиск блока с логически обзим контентом на странице
// Возможно, сделаю спецификацию в папке integration/README.txt НО это не точно
// Команда выполяет метод объекта. Имя метода == имя искомого блока, Функция метода == алгоритм по поиску данного блока
// Команда приниммает атрибуты:
// name - имя искомого блока
Cypress.Commands.add('getBlock', (name) => {
    let blocks = {
        startScreen() {cy.get('.hero-block .hero')},
        modal() {cy.get('.modal__body')},
        portfolio() {cy.get('.portfolio-block')},
        comfort() {cy.get('.comfort-block')},
        about() {cy.get('.about__body')},
        whyUs() {cy.get('.why-us__content')},
        joinUs() {cy.get('.hero-section .hero')},
    }

    blocks[name]()
})

// Данная команда пишет текст в поле ввода
// Поскольку не все поля ввода являются input, а изредко встречаются textarea, есть такая задумка
// Вводится 3-й аргумент, который в случае его истинности ветвит команду и заполняет textarea вместо input
// После выполнения этого условия функция завершается через return без использования else !!! НЕ ФАКТ ЧТО СРАБОТАЕТ !!! НЕ ПРИГОДИЛОСЬ !!!
// Аргументы:
// name - имя поля ввода
// txt - текст для печати
// ta - непустое значение этой переменной пустит функцию по пути заполнения textarea
// ??? Как же лучше реализовать поиск - через имя инпута или надпись над ним ???
// ??? Пока реализую через надпись над ним ???
// !!! До поля ввода можно дотянуться не прибегая к тэгу !!! Идея с ветвлением отложена !!!
Cypress.Commands.add('typeInTa', (name, txt) => {

    cy.contains(name).parent().children().eq(1)
      .clear()
      .type(txt)

})

// Команда на закрытие модальных окон
// Надобность сомнительная, однако приходится много раз закрывать модалки, а крестики для этого имеют очень хрупкую позицию в DOM
// На странице куча модалок, причём у некоторых display: none, однако Cypress находит их все
// Задача команды - защитить основной код от изменений в структуре DOM. Если это произойдёт, поправить в одном месте - здесь
// Аргументы:
// name - Имя модалки, которую будем закрывать. Ищётся по оглавлению, например Ошибка или Оставить заявку
Cypress.Commands.add('clickCross', (name) => {
    cy.contains(name).parent().within(() => {
        cy.get('.modal__close').click({force:true})
    })
})

// Данная команда очистит все поля ввода в нужном блоке
// Просто потому что это нудная процедура и она занимает много кода
// Ну и просто потому что могу
// Атрибуты:
// name - Имя блока
Cypress.Commands.add('clearAll', (name) => {

    cy.getBlock(name).within(() => {
        cy.get('input[type!="hidden"]').then(($numInp) => { // А вот тут интересный момент.  
            for (let i = 0; i < $numInp.length; i++) {      // Оператор "!=" позволяет нам найти ВСЕ инпуты, в которых нет типа hidden
                cy.get('input[type!="hidden"]').eq(i).clear()
            }
        })

        cy.get('textarea').then(($numInp) => {
            for (let i = 0; i < $numInp.length; i++) {
                cy.get('textarea').eq(i).clear()
            }
        })
    })

})

// Команда чтоб не вводить каждый раз логин и пароль
Cypress.Commands.add('goToPage', (adress) => {
    cy.visit(adress)
})